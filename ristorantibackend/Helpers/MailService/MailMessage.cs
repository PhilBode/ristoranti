﻿using Microsoft.AspNetCore.Http;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ristorantibackend.MailService
{
    public class MailMessage
    {
        public List<MailboxAddress> emailList { get; set; }
        public string title { get; set; }
        public string content { get; set; }

        public IFormFileCollection Attachments { get; set; }

        public MailMessage(IEnumerable<string> EmailList, string Title, string Content)
        {
            emailList = new List<MailboxAddress>();

            emailList.AddRange(EmailList.Select(x => new MailboxAddress(x,x)));
            title = Title;
            content = Content;
        }
    }
}
