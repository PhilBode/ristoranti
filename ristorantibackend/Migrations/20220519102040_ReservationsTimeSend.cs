﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RistorantiBackend.Migrations
{
    public partial class ReservationsTimeSend : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateSend",
                table: "Reservations",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateSend",
                table: "Reservations");
        }
    }
}
