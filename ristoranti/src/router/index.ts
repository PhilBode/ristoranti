import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ReservationView from '../views/ReservationView.vue'
import ContactView from '../views/ContactView.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/reservieren',
      name: 'reservieren',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: ReservationView
    },
    {
      path: '/kontakt',
      name: 'kontakt',
      component: ContactView
    }, 
    {
      path: '/dashboard/login',
      name: 'dashboard/login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('../views/Dashboard/DashboardView.vue')
    },
    {
      path: '/dashboard/user',
      name: 'benutzerverwaltung',
      component: () => import('../views/Dashboard/UserManagementView.vue')
    }
  ]
})

export default router
