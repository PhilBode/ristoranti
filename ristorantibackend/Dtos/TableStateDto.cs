﻿using System;

namespace RistorantiBackend.Dtos
{
    public class TableStateDto
    {
        public DateTime DateStart { get; set; }
    }
}
