# Ristoranti

![Logo.png](./ristoranti/src/assets/readme/Logo.png)

![MainPage.png](./ristoranti/src/assets/readme/MainPage.png)

## Beschreibung

Ein Schul-Webprojekt von Sebastian Tüttinghoff, Phillip Bode und Chris Löppenberg.
Ristoranti ist eine Reservierung-Website für Cafés oder Restaurants:
Hast du einen Lieblingsplatz in deinen lokalen Café? Oder einen Stammtisch mit Freunden in einem Restaurant?
Dann ist Ristoranti die Lösung für dich!
Hier wird dem Nutzer eine 2D Karte vom Restaurant gezeigt und man kann sich selbst den Tisch für einen bestimmten Termin aussuchen.
Natürlich wird hier gefiltert, dass man z.B. nicht alleine einen Tisch für 10 besetzen kann, auch wenn dies möglich wäre ;).

![Reserve.png](./ristoranti/src/assets/readme/Reserve.png)

Nach dem Eingang einer Reservierung wird eine Mail an den Nutzer geschickt und der Betreiber des Restaurants kann sich anmelden und diese Bestellung annehmen oder ablehnen.
Der Nutzer wird natürlich über den neuen Status der Reservierung per Mail benachrichtigt.
