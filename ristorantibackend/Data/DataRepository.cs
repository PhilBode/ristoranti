﻿using Microsoft.EntityFrameworkCore;
using RistorantiBackend.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RistorantiBackend.Data
{
    public class DataRepository : IDataRepository
    {
        private readonly DatabaseContext _context;

        public DataRepository(DatabaseContext context)
        {
            _context = context;
        }

        public Table GetTableById(int TableId)
        {
            return _context.Tables.FirstOrDefault(t => t.TableId == TableId);
        }

        public List<Reservation> GetReservationsByStatusID(int StatusID)
        {
            return _context.Reservations.FromSqlRaw(
                "SELECT * " +
                "FROM Reservations R " +
                "WHERE R.Status = " + StatusID
                ).ToList();
        }

        public List<Reservation> GetReservationsByDate(DateTime date)
        {
            return _context.Reservations.FromSqlRaw(
                "SELECT * " +
                "FROM Reservations R " +
                "WHERE(DATE(R.DateStart) = CURDATE()) AND R.Status = 1 "
                ).ToList();
        }

        public List<Reservation> GetUsedTables(DateTime date)
        {
            return _context.Reservations.FromSqlRaw(
                "SELECT * " + 
                "FROM Reservations R " +
                "WHERE R.DateStart BETWEEN '" + date.ToString("s") + "' AND '" + date.AddHours(2).ToString("s") + "' OR R.DateEnd BETWEEN '" + date.ToString("s") + "' AND '" + date.AddHours(2).ToString("s") + "'"
                ).ToList();
        }

        public List<Reservation> GetReservationsList()
        {
            return _context.Reservations.FromSqlRaw(
                "SELECT * " +
                "FROM Reservations R "
                ).ToList();
        }

        public Reservation CreateReservation(Reservation reservation)
        {
            _context.Reservations.Add(reservation);
            _context.SaveChanges();
            var reservations = GetReservationsList();
            reservation.ReservationID = reservations.Max(t => t.ReservationID);

            return reservation;
        }

        public Reservation GetReservationByID(int ReservationID)
        {
            return _context.Reservations.FirstOrDefault(res => res.ReservationID == ReservationID);
        }

        public bool UpdateReservation(Reservation reservation)
        {
            try
            {
                var res = GetReservationByID(reservation.ReservationID);
                reservation.ReservationID = res.ReservationID;

                _context.Reservations.Remove(res);
                _context.Reservations.Add(reservation);
                _context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
    }
}
