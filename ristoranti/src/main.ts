import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import store from './stores'
import BootstrapVue3 from 'bootstrap-vue-3'
import { BModal } from 'bootstrap-vue-3'

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

import "./assets/scss/custom.scss";


const app = createApp(App)

app.use(Toast);

app.use(createPinia())
app.use(router)
app.use(store)
app.use(BootstrapVue3)
app.directive('bvModal', BModal);


app.provide('hostname', "http://185.194.143.139:5001")

app.mount('#app')
