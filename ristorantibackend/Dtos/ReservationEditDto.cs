﻿using System;

namespace RistorantiBackend.Dtos
{
    public class ReservationEditDto
    {
        public int ReservationID { get; set; }
        public int Status { get; set; }
        public int TableID { get; set; }
        public int Guests { get; set; }
        public string ReservationName { get; set; }
        public string ReservationMail { get; set; }
        public string ReservationRemark { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
