﻿using RistorantiBackend.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace ristorantibackend.MailService
{
    public interface IMailSender
    {
        void SendEmail(MailMessage message);
        Task SendEmailAsync(MailMessage message);

        Task SendEmailToCustomer(Reservation reservation);



    }
}
