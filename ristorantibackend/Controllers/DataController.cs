﻿using Microsoft.AspNetCore.Mvc;
using ristorantibackend.Dtos;
using ristorantibackend.MailService;
using RistorantiBackend.Data;
using RistorantiBackend.Dtos;
using RistorantiBackend.Helpers;
using RistorantiBackend.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RistorantiBackend.Controllers
{
    [Route("api/data")]
    [ApiController]
    public class DataController : Controller
    {
        private readonly IDataRepository _dataRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMailSender _mailSender;
        private readonly JwtService _jwtService;

        public DataController(IDataRepository dataRepository, IUserRepository userRepository, JwtService jwtService, IMailSender mailSender)
        {
            _dataRepository = dataRepository;
            _userRepository = userRepository;
            _jwtService = jwtService;
            _mailSender = mailSender;
        }

        [HttpGet("users")]
        public IActionResult getUsers()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var users = _userRepository.GetUsers();

                return Ok(users);
            } catch
            {
                return Unauthorized();
            }
        }

        [HttpPost("table")]
        public IActionResult getTable(TableDto dto)
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var table = _dataRepository.GetTableById(dto.TableId);

                return Ok(table);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpPost("tablestate")]
        public IActionResult getTableState(TableStateDto dto)
        {
            try
            {
                var result = _dataRepository.GetUsedTables(dto.DateStart);
                List<Tuple<int, DateTime>> answer = new List<Tuple<int, DateTime>>();
                foreach (var reservation in result)
                {
                    answer.Add(new Tuple<int,DateTime>(reservation.TableID, reservation.DateStart));
                }

                return Ok(answer);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpGet("reservation")]
        public IActionResult getAllReservations()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservations = _dataRepository.GetReservationsList();

                return Ok(reservations);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpPost("reservation/date")]
        public IActionResult getReservationsByDate(ReservationDateDto dto)
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservations = _dataRepository.GetReservationsByDate(dto.DateStart);

                return Ok(reservations);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpPost("reservation/date/accepted")]
        public IActionResult getAcceptedReservationsByDate(ReservationDateDto dto)
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservations = _dataRepository.GetReservationsByDate(dto.DateStart);

                return Ok(reservations);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpGet("reservation/accepted")]
        public IActionResult getAcceptedReservations()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservations = _dataRepository.GetReservationsByStatusID(1);

                return Ok(reservations);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpGet("reservation/pending")]
        public IActionResult getPendingReservations()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservations = _dataRepository.GetReservationsByStatusID(0);

                return Ok(reservations);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpGet("reservation/rejected")]
        public IActionResult getRejectedReservations()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservations = _dataRepository.GetReservationsByStatusID(2);

                return Ok(reservations);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

        [HttpPost("reservation/add")]
        public IActionResult AddReservation(ReservationDto dto)
        {
            var reservation = new Reservation
            {
                Status = 0,
                ReservationName = dto.ReservationName,
                ReservationMail = dto.ReservationMail,
                ReservationRemark = dto.ReservationRemark,
                Guests = dto.Guests,
                TableID = dto.TableID,
                DateStart = dto.DateStart,
                DateEnd = dto.DateEnd,
                DateSend = DateTime.Now
            };
            reservation = _dataRepository.CreateReservation(reservation);

            
            _mailSender.SendEmailToCustomer(reservation);


            return Created("reservation added", reservation);
        }

        [HttpPost("reservation/edit")]
        public IActionResult EditReservation(ReservationEditDto dto)
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.Verify(jwt);
                var reservation = new Reservation
                {
                    ReservationID = dto.ReservationID,
                    Status = dto.Status,
                    TableID = dto.TableID,
                    Guests = dto.Guests,
                    ReservationName = dto.ReservationName,
                    ReservationMail = dto.ReservationMail,
                    ReservationRemark = dto.ReservationRemark,
                    DateStart = dto.DateStart,
                    DateEnd = dto.DateEnd
                };

                if (_dataRepository.UpdateReservation(reservation))
                {
                    _mailSender.SendEmailToCustomer(reservation);
                    return Ok(reservation);
                }
                else
                    return BadRequest(new { message = " Reservation not found " });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return Unauthorized();
            }
        }

    }
}
