﻿using RistorantiBackend.Models;

namespace ristorantibackend.MailService
{
    public class MailHelper
    {
        public static string GetEmailBodyForReservation(Reservation reservation)
        {
            var TextA = "Wir möchten Sie darüber informieren, dass Ihre Reservierung bei Ristoranti eingegangen ist.";
            var TextB = "Sie erhalten eine weitere E-Mail, sobald wir diese überprüft haben!";

            if (reservation.Status == 1)
            {
                TextA = "Wir möchten Sie darüber informieren, dass Ihre Reservierung bei Ristoranti bestätigt wurde.";
                TextB = "Wir freuen uns auf Ihren Besuch!";
            }
            if (reservation.Status == 2)
            {
                TextA = "Wir möchten Sie darüber informieren, dass Ihre Reservierung bei Ristoranti abgelehnt wurde.";
                TextB = "Versuchen Sie es mit einen anderen Zeitpunkt / Tisch nochmal.";
            }




            return "<html>" + "<head>" +
                    "<link href='https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap' rel='stylesheet'>" +
                    "<meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>" +
                    "</head>" +
                    "<body lang='EN-US' link='#0563C1' vlink='#954F72' style='tab-interval:36.0pt;word-wrap:break-word'>" +
                    "<div class='WordSection1'>" +
                    "<p class='MsoNormal' align='center' style='text-align:center'>" +
                    "<span style = 'mso-no-proof:yes'>" +
                    "<svg width='400px' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1257.52 324.27'>" +
                    "<path d='M137.85,393.57q-30.19,58.55-41.95,93.57-6.91,20.3-9.45,22.59Q83.23,513,76.54,513,71,513,68,509.27a11.36,11.36,0,0,1-2.76-7.15q0-10.81,20.74-57.61,14.74-33.64,39.18-76.52-3.23-10.59-3.23-16.13a14.9,14.9,0,0,1,6-12.45,10.38,10.38,0,0,1,6.45-2.3,21.38,21.38,0,0,1,8.3,2.3q3.45-6,25.58-44,19.81-38.49,21.89-45.18-6.22,2.78-9.91,4.84c-2.61,1.39-4.69,2.08-6.22,2.08q-3,0-5.07-3.23a10.46,10.46,0,0,1-1.61-5.76q0-11.52,15.44-20.28A167,167,0,0,1,204,217.5q3-10.83,4.84-14.29A10.87,10.87,0,0,1,219.2,197q7.83,0,13.6,8.76a168.85,168.85,0,0,1,20.63-4.84A114.91,114.91,0,0,1,272,199.29q26.73,0,40.56,15.21,13.14,14.52,13.14,35.26,0,33.89-26.51,64.07Q274.28,342,232.8,356.93q-31.8,11.52-62.69,11.52-5.3,0-16.13-.69a185.78,185.78,0,0,0,9.91,28.12q10.83,22.81,30.88,65.45,5.76,11.3,18.9,32.27,3.47,5.53,3.46,10.14,0,4.38-5.07,8.3-5.31,3.68-9,3.68-5.76,0-12.22-12.21-3.22-5.76-8.06-14.29Q165,460,137.85,393.57ZM166,343.79q8.76.46,13.14.46,47.72,0,89.19-33.41a124.38,124.38,0,0,0,29.5-34.11,48.77,48.77,0,0,0,7.15-25.35q0-13.6-8.65-20.4t-25.92-6.8a89.18,89.18,0,0,0-31.58,6q-6.22,2.31-17.74,7.61-8.07,18.43-26.28,53.35T166,343.79Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M328,356.93a8.11,8.11,0,0,1,6.46,3,8.64,8.64,0,0,1,1.84,5.07q0,4.14-6.68,13.82-24,37.11-34.11,61.08a511.4,511.4,0,0,0-19.13,50.7q-3.69,11.53-11.53,11.52a8.74,8.74,0,0,1-7.37-3.68,9.83,9.83,0,0,1-2.54-6.46q0-6.9,14.75-46.09Q281.21,415,307,377.67q8.3-12.21,12.45-16.36Q323.6,356.93,328,356.93Zm43.79-85.5q10.38,0,10.37,10.6,0,9.68-10.14,21.43-9.67,11.3-18.67,11.29a9,9,0,0,1-7.14-3.22,10.5,10.5,0,0,1-2.77-7.38q0-11.06,12.22-23.5Q364.63,271.43,371.77,271.43Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M435.61,342q14.29,0,21.44,5.07t7.14,15.44q0,13.6-10.14,26.27-9.68,12-29,19.36Q414.41,412,407.26,412q-9.67,0-9.68-8.53,0-5.75,5.54-8.75,1.38-.69,15.67-3.92a34.87,34.87,0,0,0,15.67-8.07q6.23-5.3,6.22-11.52,0-4.85-2.76-7.15c-1.85-1.53-4.61-2.3-8.3-2.3q-13.37,0-29.5,10.83-18.67,12.22-18.67,29,0,12,12.22,30,13.59,19.59,13.59,33.19,0,20.28-18,31.8a45.25,45.25,0,0,1-25.12,7.15q-22.12,0-33.65-15.9-8.07-11.29-8.07-26.74,0-18,9.22-30.19,6-8.07,12.45-8.07,5.76,0,5.76,4.61,0,1.86-2.08,5.53-5.3,9.91-5.3,23.28a25.64,25.64,0,0,0,8.07,19.13,22.86,22.86,0,0,0,16.36,6.91q9,0,14.06-6c2.77-3.37,4.15-7.68,4.15-12.9q0-9.69-11.06-25.12-15.21-21.21-15.21-38.49,0-23,25.12-40.8Q408.41,342,435.61,342Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M534.25,346.56a374.7,374.7,0,0,0-27.65,52.08q-18.21,41-18.21,61.31,0,5.76,2.42,8.76c1.61,2,4,3,7.26,3q11.52,0,27.89-21.66a278,278,0,0,0,18.43-26.74q4.38-7.6,9.45-7.6a6.89,6.89,0,0,1,5.07,2.07,6.32,6.32,0,0,1,1.85,4.15q0,8.54-22.36,38-26.73,35.49-48.86,35.49-11.75,0-19.59-10.83-4.83-6.92-4.84-19.59,0-27.88,16.14-65,3.92-9.22,21.66-44.48a41.54,41.54,0,0,1-9.45,1.61,15.81,15.81,0,0,1-8.53-2.3q-5.07-3.23-5.07-8.07,0-5.76,6.69-9.22,1.83-.92,17.05-3a121.27,121.27,0,0,0,16.13-4.37q8.07-14.52,26.28-39.41,10.14-13.83,14.06-18,5.52-5.76,9.68-5.76,3,0,6,3.22a9,9,0,0,1,2.76,6q0,6-12.9,23.73a216.8,216.8,0,0,0-12.68,18.67q25.59-6.92,36.65-6.91,12.66,0,12.67,10.6a8.43,8.43,0,0,1-4.38,7.84q-2.31,1.38-12,2.3-3.93-.22-34.8,8.76Q542.54,343.79,534.25,346.56Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M654.1,346.33q18.89,0,18.9,23.05Q673,400,644.65,447,613.76,498,585,498q-13.37,0-19.59-12.45a32.2,32.2,0,0,1-3.23-15q0-29.49,20.28-64.07,18.9-32,44.48-49.78Q641.89,346.34,654.1,346.33Zm-24,87.12q-5.76-4.62-5.76-7.15,0-1.85,7.37-10.14,20.75-23.28,20.74-37.57a6.89,6.89,0,0,0-1.15-3.68,6.1,6.1,0,0,0-5.76-3.46q-7.38,0-21.66,14.29-18.45,18.43-28.12,38-11.29,22.35-11.29,37.1a11.87,11.87,0,0,0,2.07,6.92,9,9,0,0,0,7.6,4.14q11.07,0,23.74-16.59a71,71,0,0,0,9-14.29C629.21,436.14,630.28,433.6,630.13,433.45Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M716.55,419.16a449.78,449.78,0,0,1,52.09-47.25,215.26,215.26,0,0,1,22.59-15.44q6-3.47,7.6-3.46,3.46,0,6.92,5.07a8.76,8.76,0,0,1,2.07,5.07q0,4.16-8.76,11.3-23.5,18.66-23.51,28.11c0,3.54,2,5.3,6,5.3q9.9,0,34.11-16.36c2.76-1.84,5.07-2.77,6.91-2.77a5.32,5.32,0,0,1,3.69,1.85,6.17,6.17,0,0,1,2.07,4.38q0,6.92-19.36,21.2-20.28,15.21-38.49,15.21a24.91,24.91,0,0,1-13.59-3.92q-5.31-3.22-5.31-11.52,0-5.76,4.84-13.14Q738.22,415.7,713.56,447q-14.29,19.36-28.35,47.71-5.3,10.61-13.14,10.6-8.52,0-8.52-8.06,0-7.85,13.36-42,19.82-50.7,41.49-78.59,13.36-17.28,22.35-17.28a9.19,9.19,0,0,1,5.53,1.84,7,7,0,0,1,3,5.76q0,3.46-4.61,9.68-.93,1.15-12.21,15.44A121.31,121.31,0,0,0,716.55,419.16Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M886.64,457a174,174,0,0,1-15,22.59q-14.52,18-28.81,18-12.45,0-19.36-11.76a35.39,35.39,0,0,1-4.38-18q0-25.59,16.14-56.47,18-34.33,43.32-52.78,16.6-12,32-12a22.81,22.81,0,0,1,16.13,6q5.53,5.31,5.53,12,0,5.07-3.92,14c2.31,3.23,3.46,6.08,3.46,8.53q0,4.85-6.45,15.9-6.69,11.3-13.37,30-7.14,19.83-7.14,31.81,0,9.68,5.76,15.9,1.6,1.62,9.45,5.3,5.76,2.77,5.76,9.68a9,9,0,0,1-3.23,7.15,11.9,11.9,0,0,1-8.06,3q-12.45,0-21.44-11.06-7.38-9-7.37-22.59Q885.72,467.55,886.64,457Zm28.81-75.82a22.94,22.94,0,0,0-6-7.49,11.36,11.36,0,0,0-6.92-2.42q-17.52,0-38,30.65-22.57,33.88-22.58,57.39,0,5.76,2.19,8.76a7.62,7.62,0,0,0,6.57,3q11.05,0,25.81-23.51,8.29-12.9,20.51-38,8.07-16.37,11.75-21.67A33.65,33.65,0,0,1,915.45,381.13Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M983.67,415.93c2-1.84,3.91-3.69,5.76-5.53q48.17-47,64.53-47,9,0,9,9.91,0,6.91-8.07,16.37-44,51.39-44,90.57a46.33,46.33,0,0,0,6.92,24.2c2.15,3.54,3.22,6.3,3.22,8.3a7.26,7.26,0,0,1-2.76,5.3,9.43,9.43,0,0,1-7.15,3.22q-9.45,0-15.9-12Q989,498,989,481.84q0-31.11,23.51-68.91-31.35,20.06-58.31,71-6.92,12.21-14.75,12.21a8.41,8.41,0,0,1-6.46-2.76,11.24,11.24,0,0,1-3.22-8.3q0-12.45,28.34-65.22,15.23-28.58,24-41.49,8.76-12.66,15-12.67a8.19,8.19,0,0,1,5.77,2.76,10.17,10.17,0,0,1,3.22,7.38q0,5.07-6.68,14.52A246,246,0,0,0,983.67,415.93Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M1146.84,346.56a375.78,375.78,0,0,0-27.66,52.08Q1101,439.68,1101,460c0,3.84.8,6.76,2.42,8.76s4,3,7.26,3q11.52,0,27.88-21.66A276.3,276.3,0,0,0,1157,423.3q4.38-7.6,9.45-7.6a6.91,6.91,0,0,1,5.07,2.07,6.3,6.3,0,0,1,1.84,4.15q0,8.54-22.35,38-26.74,35.49-48.86,35.49-11.76,0-19.59-10.83-4.85-6.92-4.84-19.59,0-27.88,16.13-65,3.92-9.22,21.67-44.48a41.54,41.54,0,0,1-9.45,1.61,15.79,15.79,0,0,1-8.53-2.3q-5.07-3.23-5.07-8.07,0-5.76,6.68-9.22,1.84-.92,17.06-3a121.68,121.68,0,0,0,16.13-4.37q8.07-14.52,26.27-39.41,10.14-13.83,14.06-18,5.53-5.76,9.68-5.76c2,0,4,1.07,6,3.22a8.92,8.92,0,0,1,2.77,6q0,6-12.91,23.73a216.27,216.27,0,0,0-12.67,18.67q25.58-6.92,36.64-6.91,12.67,0,12.68,10.6a8.43,8.43,0,0,1-4.38,7.84q-2.31,1.38-12,2.3-3.92-.22-34.8,8.76Q1155.14,343.79,1146.84,346.56Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M1238.1,356.93q-4.38,0-8.52,4.38-4.15,4.14-12.45,16.36-25.82,37.33-37.33,68.22Q1165,485.07,1165.05,492a9.82,9.82,0,0,0,2.53,6.46,8.75,8.75,0,0,0,7.38,3.68q7.83,0,11.52-11.52a511.4,511.4,0,0,1,19.13-50.7q10.14-24,34.11-61.08,6.67-9.68,6.68-13.82a8.58,8.58,0,0,0-1.84-5.07A8.09,8.09,0,0,0,1238.1,356.93Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M1287.72,266.44c0,.36,4.24,47.29-49.89,74,0,0-16.76-40.57,17.65-71.53-10.12,12.05-16.53,28.67-12,51.25C1243.52,320.13,1277.63,300.6,1287.72,266.44Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M1312.37,313.68c14.22-35.53-8.84-73.21-8.84-73.21s-30,6.86-48,28.41c-34.41,31-17.65,71.53-17.65,71.53s-20.22-38.89-.24-71.37c18.84-30.63,70.79-45.4,70.79-45.4S1337.9,276,1312.37,313.68Z' transform='translate(-65.25 -196.98)'/>" +
                    "<path d='M1312.37,313.68c-10.5,15.51-30.32,28.52-65.36,34.39,0,0,87.68-27.86,56.52-107.6C1303.53,240.47,1326.59,278.15,1312.37,313.68Z' transform='translate(-65.25 -196.98)'/>" +
                    "</svg>" +
                    "</span>" +
                    "</p>" +
                    "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "</span>" + "</p>" +
                    "<div align='center'>" +
                    "<table class='MsoTableGrid' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt; mso - border - insideh:none; mso - border - insidev:none'>" +
                     "<tbody>" + "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;height: 429.95pt'>" +
                      "<td width='663' valign='top' style='width:497.5pt;padding:0cm 5.4pt 0cm 5.4pt;height: 429.95pt'>" +
                      "<p class='MsoNormal' align='center' style='text-align:center'>" + "<b>" + "<span lang='DE' style='font-size:14.0pt;font-family:Nunito;mso-ansi-language:DE'>" + $"Hey {reservation.ReservationName}, " + "</span>" + "</b>" + "</p>" +
                      "<p class='MsoNormal' align='center' style='text-align:center'>" + "<b>" + "<span lang='DE' style='font-size:14.0pt;font-family:Nunito;mso-ansi-language:DE'>" + "</span>" + "</b>" + "</p>" +
                      "<p class='MsoNormal' align='center' style='text-align:center'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + TextA + "</span>" + "</p>" +
                      "<p class='MsoNormal' align='center' style='text-align:center'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + TextB + "</span>" + "</p>" +
                      "<p class='MsoNormal' align='center' style='text-align:center'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "</span>" + "</p>" +
                      "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "<span style='mso-spacerun:yes'>" + "</span>" + "</span>" + "<b>" + "<span lang='DE' style='font-size:14.0pt;font-family:Nunito;mso-ansi-language:DE'>" + " Reservierungsdetails:" + "</span>" + "</b>" + "</p>" +
                      "<table class='MsoTableGrid' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>" +
                       "<tbody>" + "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "Reservierung #:" + "</span>" + "</b>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:393.05pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + reservation.ReservationID + "</span>" + "</p>" +
                        "</td>" +
                       "</tr>" +
                       "<tr style='mso-yfti-irow:1'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "Datum:" + "</span>" + "</b>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:393.05pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + reservation.DateStart.ToShortDateString() + "</p>" +
                        "</td>" +
                       "</tr>" +
                       "<tr style='mso-yfti-irow:2'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "Zeit:" + "</span>" + "</b>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:393.05pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + $" {reservation.DateStart.ToString("HH:mm")} - {reservation.DateEnd.ToString("HH:mm")}" + "</span>" + "</p>" +
                        "</td>" +
                       "</tr>" +
                       "<tr style='mso-yfti-irow:3'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "Gäste:" + "</span>" + "</b>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:393.05pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + reservation.Guests + "</span>" + "</p>" +
                        "</td>" +
                       "</tr>" +
                       "<tr style='mso-yfti-irow:4;mso-yfti-lastrow:yes'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "Anmerkung:" + "</span>" + "</b>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:393.05pt;padding:0cm 5.4pt 0cm 5.4pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + reservation.ReservationRemark + "</span>" + "</p>" +
                        "</td>" +
                       "</tr>" +
                      "</tbody>" + "</table>" +
                      "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-size:14.0pt;font-family:Nunito;mso-ansi-language:DE'>" + "</span>" + "</b>" + "</p>" +
                      "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-size:14.0pt;font-family:Nunito;mso-ansi-language:DE'>" + "Ihre Kontaktdaten: " + "</span>" + "</b>" + "</p>" +
                      "<div align='center'>" +
                      "<table class='MsoTableGrid' border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border:none;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:none;mso-border-insidev:none'>" +
                       "<tbody>" + "<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:20.2pt'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.2pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "Name:" + "</span>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:392.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.2pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + reservation.ReservationName + "</span>" + "</p>" +
                        "</td>" +
                       "</tr>" +
                       "<tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes;height:20.2pt'>" +
                        "<td width='124' valign='top' style='width:92.85pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.2pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "E-Mail" + "</span>" + "</p>" +
                        "</td>" +
                        "<td width='524' valign='top' style='width:392.65pt;padding:0cm 5.4pt 0cm 5.4pt;height:20.2pt'>" +
                        "<p class='MsoNormal'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + reservation.ReservationMail + "</span>" + "</p>" +
                        "</td>" +
                       "</tr>" +
                      "</tbody>" + "</table>" +
                      "</div>" +
                      "<p class='MsoNormal'>" + "<b>" + "<span lang='DE' style='font-size:14.0pt;font-family:Nunito;mso-ansi-language:DE'>" + "</span>" + "</b>" + "</p>" +
                      "</td>" +
                     "</tr>" +
                    "</tbody>" + "</table>" +
                    "</div>" +
                    "<p class='MsoNormal' align='center' style='text-align:center'>" + "<span lang='DE' style='font-family:Nunito;mso-ansi-language:DE'>" + "</span>" + "</p>" +
                    "</div>" +
                    "</body>" + "</html>";

        }
    }
}
